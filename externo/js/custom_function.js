function initFunction() {}
initFunction.prototype.header_fixed = function header_fixed() {
    $(".header_sticky").toggleClass("header_map Fixed");
};
initFunction.prototype.selectBox = function selectBox() {
    return $(".selectbox").selectbox();
};

initFunction.prototype.initTooltip = function initTooltip() {
    $('[data-toggle="tooltip"]').tooltip();
};

initFunction.prototype.bgBody = function bgBody() {
    $('#dash').toggleClass('bg_color_2');
};

initFunction.prototype.initHeader = function initHeader() {
    // Sticky nav + scroll to top
    var $headerStick = $('.header_sticky');
    var $toTop = $('#toTop');
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 1) {
            $headerStick.addClass("sticky");
        } else {
            $headerStick.removeClass("sticky");
        }
        if ($(this).scrollTop() != 0) {
            $toTop.fadeIn();
        } else {
            $toTop.fadeOut();
        }
    });
    $toTop.on("click", function() {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });

    // Menu
    $('a.open_close').on("click", function() {
        $('.main-menu').toggleClass('show');
        //$('.layer').toggleClass('layer-is-visible');
        $('header.static').toggleClass('header_sticky sticky');
        $('body').toggleClass('');
    });
    $('a.show-submenu').on("click", function() {
        $(this).next().toggleClass("show_normal");
        $('a.open_close').click();
        $('a.open_close').toggleClass('active');
    });

    // Hamburger icon
    var toggles = document.querySelectorAll(".cmn-toggle-switch");
    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    }

    function toggleHandler(toggle) {
        toggle.addEventListener("click", function(e) {
            e.preventDefault();
            (this.classList.contains("active") === true) ? this.classList.remove("active"): this.classList.add('active');
        });
    }
};

initFunction.prototype.getValue = function getValue(element) {
    return $('#' + element).val();
};

initFunction.prototype.scrollAutocomplete = function scrollAutocomplete(i, key) {
    var top = document.getElementById('autocomplete').scrollTop;
    var option = document.getElementById('auto-option' + i);
    setTimeout(() => {
        if (i >= 2 && key == 1) {
            document.getElementById('autocomplete').scrollTop = top + option.offsetHeight;
        } else if (key == 2) {
            if (top > document.getElementById('auto-option' + i).offsetTop) {
                document.getElementById('autocomplete').scrollTop = top - option.offsetHeight;
            }

        }
    }, 300);
};

initFunction.prototype.selectBoxRefresh = function selectBoxRefresh() {
    return $('.selectbox').selectbox('refresh');
};

initFunction.prototype.initTab = function initTab() {
    return $('#v-tabs-tab a').on('click', function(e) {
        e.preventDefault();
        $(this).tab('show');
    });
};

var initFunctions = new initFunction;
export { initFunctions };