function initFunction(){};
initFunction.prototype.showModal=function showModal(id){
    $("#"+id).modal('show');
}

initFunction.prototype.initTooltips=function initTooltips(){
    $('body>.tooltip').remove();
    $("[data-toggle='tooltip']").tooltip('hide');
	$('[data-toggle="tooltip"]').tooltip();
    
}

var initFunctions2= new initFunction();

export{ initFunctions2 };