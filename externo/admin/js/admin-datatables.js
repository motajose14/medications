// Call the dataTables jQuery plugin
function initCoupons() {}
initCoupons.prototype.initTable = function initTable() {
$(document).ready(function() {
  $('#coupons').DataTable();
  $('[data-toggle="tooltip"]').tooltip();
  console.log('Llena');
  
});
};

initCoupons.prototype.clearTable = function clearTable() {
  $(document).ready(function() {
    $("#coupons").dataTable().fnDestroy();  
    $('#coupons').DataTable().clear().draw();
    console.log('Limpia');
    
  });
  };
var initCouponsTable = new initCoupons();

function initReports() {}
initReports.prototype.initTable = function initTable() {
$(document).ready(function() {
  $("#reports").dataTable().fnDestroy();  
  var table= $('#reports').DataTable( {
    buttons: [
         'csv', 'excel', 'pdf', 'print'
    ],
    'destroy':true
} );
$('[data-toggle="tooltip"]').tooltip()
table.buttons().container()
        .appendTo( '#reports_wrapper .col-md-6:eq(0)' );
});
};
var initReportsTable = new initReports();

function initTooltips() {}
initTooltips.prototype.initTooltip = function initTooltip() {
  $(document).ready(function() {
    $("[data-toggle='tooltip']").tooltip('hide');
    $('[data-toggle="tooltip"]').tooltip()
  });
  };
  var initAllTooltips = new initTooltips();

  function functions() {}
  functions.prototype.md5 = function md5(param) {
    if(param!='' && param !=null){
      var md5_hash= calcMD5(param);
      return md5_hash;
    }
  };
  var initFunctions = new functions();


export { initCouponsTable,initReportsTable,initAllTooltips,initFunctions };
