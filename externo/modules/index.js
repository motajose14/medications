function initModule(){};
initModule.prototype.initResult= function initResult(){
  $("#results").stick_in_parent({
    offset_top: 0
  });
}

initModule.prototype.initToTop=function initToTop(){
  var $toTop = $('#toTop');
	$(window).on("scroll", function () {

		if ($(this).scrollTop() != 0) {
			$toTop.fadeIn();
		} else {
			$toTop.fadeOut();
		}
	});
	$toTop.on("click", function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
	});

}

var initModuleFunctions= new initModule();

export {initModuleFunctions}
