import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule, GoogleMapsAPIWrapper, InfoWindowManager, MarkerManager } from '@agm/core';
import { FooterComponent } from './components/footer/footer.component';
import { ResultsComponent } from './pages/results/results.component';
import { FilterComponent } from './components/modals/filter/filter.component';
import { HttpClientModule } from '@angular/common/http';
import { DetailsComponent } from './pages/details/details.component';
import { HeaderComponent } from './components/header/header.component';
import { PayComponent } from './pages/pay/pay.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { PrintComponent } from './pages/print/print.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavadminComponent } from './pages/navadmin/navadmin.component';
import { WhislistComponent } from './pages/whislist/whislist.component';
import { GetpriceComponent } from './components/modals/getprice/getprice.component';
import { TellusComponent } from './components/modals/tellus/tellus.component';
import { FormSearchComponent } from './components/form-search/form-search.component';
import { AccountComponent } from './pages/account/account.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { DetailCertificateComponent } from './pages/detail-certificate/detail-certificate.component';
import { CartComponent } from './pages/cart/cart.component';
import { DetailOrderComponent } from './pages/detail-order/detail-order.component';
import { AddshippingComponent } from './components/modals/addshipping/addshipping.component';
import { ReviewComponent } from './components/review/review.component';
import { AddReviewsComponent } from './components/modals/add-reviews/add-reviews.component';
import { CertificatePdfComponent } from './components/certificate-pdf/certificate-pdf.component';
import { NgxStripeModule } from 'ngx-stripe';
import { AutocompleteModule } from './components/autocomplete/autocomplete.module';
import { RatingsComponent } from './components/ratings/ratings.component';
import { ResetComponent } from './pages/reset/reset.component';
import { PasswordMatchDirective } from './directive/password-match.directive';
import { SelectComponent } from './components/select/select.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    ResultsComponent,
    FilterComponent,
    DetailsComponent,
    HeaderComponent,
    PayComponent,
    PrintComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    ForgotComponent,
    RegisterComponent,
    NavadminComponent,
    WhislistComponent,
    GetpriceComponent,
    TellusComponent,
    FormSearchComponent,
    AccountComponent,
    ProfileComponent,
    ReviewsComponent,
    DetailCertificateComponent,
    CartComponent,
    DetailOrderComponent,
    AddshippingComponent,
    ReviewComponent,
    AddReviewsComponent,
    CertificatePdfComponent,
    RatingsComponent,
    ResetComponent,
    PasswordMatchDirective,
    SelectComponent,
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCXX4ssuXm2eBH15iDTlA5TOLkXhARJvNY',
      libraries:['places']
    }),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxStripeModule.forRoot('pk_test_5Yd0eA1FXrVMBOuciO00esoL00VbjCOtv6'),
    AutocompleteModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    InfoWindowManager,
    GoogleMapsAPIWrapper,
    MarkerManager,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
