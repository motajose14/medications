import { Component, OnInit, OnDestroy } from '@angular/core';
import { initFunctions } from '../../../../externo/js/custom_function';
import { initFunctions2 } from '../../../../externo/js/myfunctions';
import { IsLoginService } from 'src/app/services/isLogin/is-login.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isLog = false;

  items = 0;
  itemsSubcription: Subscription;

  constructor(
    private isLogin: IsLoginService,
    private router: Router,
    private shoppingCart: ShoppingCartService
  ) { }

  ngOnInit() {
    initFunctions.initHeader();
    initFunctions2.initTooltips();
    this.isLog = this.isLogin.getLoggin();
    this.itemsSubcription = this.shoppingCart.itemsCart().subscribe( (num) => {
      this.items = num;
    });

  }

  getLink(){
    initFunctions2.initTooltips();
  }

  ngOnDestroy(){
    initFunctions2.initTooltips();
    this.itemsSubcription.unsubscribe();
  }

  logout(){
    localStorage.setItem('loggin','false');
    localStorage.removeItem('data');
    this.isLogin.setLoggin(false);
    this.isLog=false;
    this.router.navigate(['/login']);
  }

}
