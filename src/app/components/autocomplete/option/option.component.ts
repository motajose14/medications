import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataApiService } from '../../../services/dataApi/data-api.service';

@Component({
  selector: 'app-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.css']
})
export class OptionComponent implements OnInit {

  @Input() data: any[] = [];
  @Output() item: EventEmitter<any[]> = new EventEmitter<any[]>();
  focus = 0;

  constructor(
    private dataApi: DataApiService
  ) { }

  ngOnInit() {

  }

  getItem(item) {
    this.item.emit(item);
  }

}
