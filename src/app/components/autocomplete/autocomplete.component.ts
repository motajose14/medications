import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';
import { initFunctions } from '../../../../externo/js/custom_function';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit, OnChanges {

  display = false;
  data = [];
  @Input() value: string;
  @Input() keyPress: number;
  time = null;
  @Output() setItem: EventEmitter<any[]> = new EventEmitter<any[]>();
  count = -1;

  constructor(
    private dataApi: DataApiService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(event) {
    if (event.hasOwnProperty('value')) {
      this.count = -1;
      if (!event.value.firstChange) {
        if (event.value.currentValue.length > 2) {
          this.value = event.value.currentValue;
          this.getAutocomplete();
        }
      }
    }
    if (event.hasOwnProperty('keyPress')) {
      let dataFocus = this.data;
      if (!event.keyPress.firstChange) {
          dataFocus = dataFocus.map( (item) => {
            item.focus = false;
            return item;
          });
          setTimeout(() => {
            if (this.data.length > 0) {
              if (event.keyPress.currentValue === 40) {
                this.count++;
                if (this.count >= this.data.length) {
                  this.count = this.data.length - 1;
                }
                this.data[this.count].focus = true;
                initFunctions.scrollAutocomplete(this.count, 1);
              } else if (event.keyPress.currentValue === 38) {
                this.count--;
                if (this.count < 0) {
                  this.count = 0;
                }
                this.data[this.count].focus = true;
                initFunctions.scrollAutocomplete(this.count, 2);
              } else if (event.keyPress.currentValue === 13) {
                  this.getItem(this.data[this.count]);
                  clearTimeout(this.time);
                  this.data = [];
              } else if (event.keyPress.currentValue === 27) {
                clearTimeout(this.time);
                this.data = [];
              }
            }
          }, 200);
      }
    }
  }

  getAutocomplete() {
    clearTimeout(this.time);
    this.time = setTimeout(() => {
      if (this.value.length !== 0) {
        this.dataApi.getAutocomplete(this.value)
        .subscribe((data) => {
          this.data = data.map((item) => {
            const value = {
              label: item.commonname,
              id: item.dnd ? item.dnd : item.did,
              focus: false
            };
            return value;
          });
        });
      } else {
        this.data = [];
      }
    }, 300);

  }

  getItem(event) {
    this.setItem.emit(event);
    this.data = [];
  }

}
