import { Component, OnInit } from '@angular/core';
import { IsLoginService } from 'src/app/services/isLogin/is-login.service';
import { IDataUser } from './../../../interfaces/interfaces';
import { DataApiService } from './../../../services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
 
@Component({
  selector: 'app-tellus',
  templateUrl: './tellus.component.html',
  styleUrls: ['./tellus.component.css']
})
export class TellusComponent implements OnInit {

  idUser="";
  userData:IDataUser;
  formTell: FormGroup;
  drugRequired:boolean=false;
  qtyRequired:boolean=false;
  priceRequired:boolean=false;
  commentRequired:boolean=false;
  drugShow:boolean=false;
  qtyShow:boolean=false;
  priceShow:boolean=false;
  commentShow:boolean=false;

  constructor(
    private isLoginServices: IsLoginService,
    public dataApiServices: DataApiService,
    public toast: ToastrService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formTell=this.fb.group({
      name:[null,[Validators.required, Validators.minLength(3),Validators.pattern(/^[a-zA-Z ]+$/)]],
      email:[null,[Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      reason:['',[Validators.required]],
      qty:[null],
      price:[null],
      drug_name:[null],
      comments:[null]
    });
    if(this.isLoginServices.getLoggin()){
      this.idUser=this.isLoginServices.getRow();
      var form=new FormData();
      form.append('row',this.idUser);
      this.dataApiServices.getDataUser(form).subscribe((data)=>{
        this.userData=data;
      },error=>{
         this.toast.error('Error','Error receiving data');
      });
    }
  }

  saveTellUs(){
    if(this.formTell.valid){
      Swal.fire({
        title: 'Success!',
        text: 'This address will be deleted',
        icon: 'success',
        confirmButtonText: 'Close',
      });
    }else{
      this.toast.error('Error','You must fill the required fields');
    }
  }

  changeReason(){
    this.resetValidators();
    this.formTell.controls['drug_name'].setErrors(null);
    this.formTell.controls['qty'].setErrors(null);
    this.formTell.controls['price'].setErrors(null);
    if(this.formTell.get('reason').value=="Submit Pricing"){
      this.drugRequired=true;
      this.qtyRequired=true;
      this.priceRequired=true;
      this.drugShow=true;
      this.qtyShow=true;
      this.priceShow=true;      
      this.formTell.controls['drug_name'].setValidators([Validators.required]);
      this.formTell.controls['qty'].setValidators([Validators.required]);
      this.formTell.controls['price'].setValidators([Validators.required]);
      this.formTell.controls['comments'].setErrors(null);

    }else if(this.formTell.get('reason').value=="Suggest New Drug"){
      this.drugRequired=true;
      this.drugShow=true;
      this.qtyShow=true;
      this.priceShow=true;
      this.formTell.controls['drug_name'].setValidators([Validators.required]);
      this.formTell.controls['qty'].setErrors(null);
      this.formTell.controls['price'].setErrors(null);
      this.formTell.controls['comments'].setErrors(null);
    }else if(this.formTell.get('reason').value=="Suggest Edit"){
      this.drugRequired=true;
      this.drugShow=true;
      this.commentRequired=true;
      this.commentShow=true;
      this.formTell.controls['drug_name'].setValidators([Validators.required]);
      this.formTell.controls['comments'].setValidators([Validators.required]);
      this.formTell.controls['price'].setErrors(null);
      this.formTell.controls['qty'].setErrors(null);
    }else if(this.formTell.get('reason').value=="Other"){
      this.commentRequired=true;
      this.commentShow=true;
      this.formTell.controls['comments'].setValidators([Validators.required]);
      this.formTell.controls['qty'].setErrors(null);
      this.formTell.controls['price'].setErrors(null);
      this.formTell.controls['drug_name'].setErrors(null);
    }
  }

  resetValidators(){
    this.drugRequired=false;
    this.qtyRequired=false;
    this.commentRequired=false;
    this.priceRequired=false;
    this.drugShow=false;
    this.qtyShow=false;
    this.commentShow=false;
    this.priceShow=false;
    this.formTell.controls['drug_name'].setValidators(null);
    this.formTell.controls['qty'].setValidators(null);
    this.formTell.controls['price'].setValidators(null);
    this.formTell.controls['comments'].setValidators(null);
    
   
  }

}
