import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @ViewChild('cancel', { static: false }) closeButton: ElementRef;
  // @Output() values: EventEmitter<any> = new EventEmitter<any>();
  distances = [];
  /* ratings=[]; */
  pharmacyTypes = [];
  valueDistance = 5;
  valueRating = 'any';
  valuePTypes = 'Any';
  date1: any;
  date2: any;

  constructor(
    private dataApi: DataApiService
  ) { }

  ngOnInit() {

    this.getFilter();

  }

  getFilter() {
    this.distances = this.dataApi.distances;
    this.date1 = this.dataApi.dateFrom;
    this.date2 = this.dataApi.dateTo;
    /* this.ratings=this.dataApi.sellerRating; */
    this.pharmacyTypes = this.dataApi.pharmacyTypes;
    for (let a=0 ;a < this.distances.length; a++) {
      if (this.distances[a].checked) {
        this.valueDistance = this.distances[a].distance;
      }
    }
   /*  for(let a=0;a<this.ratings.length;a++){
      if( this.ratings[a].checked){
        this.valueRating=this.ratings[a].value;
      }
    } */
    for(let a=0;a<this.pharmacyTypes.length;a++){
      if (this.pharmacyTypes[a].checked) {
        this.valuePTypes=this.pharmacyTypes[a].title;
      }
    }

  }

  closeModal(){
    this.closeButton.nativeElement.click();
  }

  resetForm() {
    this.dataApi.reserDate();
    this.date1 = this.dataApi.dateFrom;
    this.date2 = this.dataApi.dateTo;
    for(let a=0;a<this.distances.length;a++){
      if (this.distances[a].distance === 5) {
        this.distances[a].checked = true;
      } else {
        this.distances[a].checked = false;
      }
    }
    /* for(let a=0;a<this.ratings.length;a++){
      if(this.ratings[a].value=='any'){
        this.ratings[a].checked=true;
      }else{
        this.ratings[a].checked=false;
      }
    } */
    for(let a=0;a<this.pharmacyTypes.length;a++){
      if (this.pharmacyTypes[a].title === 'Any') {
        this.pharmacyTypes[a].checked = true;
      } else {
        this.pharmacyTypes[a].checked = false;
      }
    }
    this.dataApi.distances= Object.assign([], this.distances);
    /* this.dataApi.sellerRating= Object.assign([],this.ratings); */
    this.dataApi.pharmacyTypes= Object.assign([],this.pharmacyTypes);
    this.valueDistance=5;
    this.valueRating='any';
    this.valuePTypes='Any';
    this.date1="";
    this.date2="";
  }

  sendFilter(){
    /* let res = {distance:this.valueDistance,rating:this.valueRating, pharmacyTypes:this.pharmacyTypes} */
    // this.values.emit(res);
    this.dataApi.distances = Object.assign([], this.distances);
    /* this.dataApi.sellerRating= Object.assign([],this.ratings); */
    this.dataApi.pharmacyTypes = Object.assign([], this.pharmacyTypes);
    this.dataApi.dateFrom = this.date1;
    this.dataApi.dateTo = this.date2;
    this.closeModal();
  }

  changedFilter() {
    for(let a=0;a<this.distances.length;a++){
      if(this.distances[a].distance === this.valueDistance) {
        this.distances[a].checked = true;
      } else {
        this.distances[a].checked = false;
      }
    }
    /* for(let a=0;a<this.ratings.length;a++){
      if(this.ratings[a].value==this.valueRating){
        this.ratings[a].checked=true;
      }else{
        this.ratings[a].checked=false;
      }
    } */

    for(let a=0;a<this.pharmacyTypes.length;a++){
      if (this.pharmacyTypes[a].title === this.valuePTypes) {
        this.pharmacyTypes[a].checked = true;
      } else {
        this.pharmacyTypes[a].checked = false;
      }
    }
  }

}
