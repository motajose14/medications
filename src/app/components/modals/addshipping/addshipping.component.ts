import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';

@Component({
  selector: 'app-addshipping',
  templateUrl: './addshipping.component.html',
  styleUrls: ['./addshipping.component.css']
})
export class AddshippingComponent implements OnInit {

  formAddress:FormGroup;
  @Output() result:EventEmitter<any>=new EventEmitter<any>();
  @Input() idUser;
  @ViewChild('close',{static:false}) close;
  constructor(
    public fb: FormBuilder,
    public toast:ToastrService,
    public dataApi:DataApiService
  ) { }

  ngOnInit() {
    this.formAddress=this.fb.group({
      address_type:['Home',[]],
      street1:[null,[Validators.required,Validators.minLength(3)]],
      street2:[null,[]],
      city:[null,[Validators.required,Validators.minLength(3)]],
      state:['Alabama',[]],
      zip_code:[null,[Validators.required,Validators.minLength(2)]],
      status:['0',[]]
    }); 
    
  }

  sendAddress(){
    if(this.formAddress.valid){
      var form= new FormData();
      form.append('id',this.idUser);
      for(const control in this.formAddress.value){
        form.append(control,this.formAddress.controls[control].value);
      }
      this.dataApi.saveAddress(form).subscribe((data)=>{
        if(data.result){
          this.toast.success('Success',data.msg);

          this.result.emit({
            reload:true
          });

          this.close.nativeElement.click();
          setTimeout(() => {
            this.formAddress.controls['address_type'].setValue('Home');
            this.formAddress.controls['state'].setValue('Alabama');
            this.formAddress.controls['status'].setValue('0');
          }, 500);

        }else{
          this.toast.error('Error',data.msg);
        }
      },err=>{
        this.toast.error('Error','Error sending form');
      });
    }else{
      this.toast.error('Error','You must fill in all fields');
    }
  }

}
