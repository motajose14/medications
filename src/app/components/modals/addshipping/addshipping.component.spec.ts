import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddshippingComponent } from './addshipping.component';

describe('AddshippingComponent', () => {
  let component: AddshippingComponent;
  let fixture: ComponentFixture<AddshippingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddshippingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddshippingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
