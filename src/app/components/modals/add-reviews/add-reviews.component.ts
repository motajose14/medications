import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataApiService } from '../../../services/dataApi/data-api.service';

@Component({
  selector: 'app-add-reviews',
  templateUrl: './add-reviews.component.html',
  styleUrls: ['./add-reviews.component.css']
})
export class AddReviewsComponent implements OnInit, OnChanges {

  @Input() reset = false;
  @Input() orderdet_id = '';
  @Input() user_id = false;
  tc = false;

  formReview = new FormGroup({
    orderdet_id: new FormControl(null, [Validators.required]),
    user_id: new FormControl(null, [Validators.required]),
    t_condition: new FormControl(null, [Validators.required]),
    length: new FormControl('Less than 1 month', [Validators.required]),
    effectiveness: new FormControl(null, [Validators.required]),
    easy_use: new FormControl(null, [Validators.required]),
    satisfaction: new FormControl(null, [Validators.required]),
    overall: new FormControl('Yes', [Validators.required]),
    comments: new FormControl(null, [Validators.required]),
    insurance: new FormControl('Yes', [Validators.required]),
    strength: new FormControl(null, [Validators.required]),
    quantity: new FormControl(null, [Validators.required]),
    cost: new FormControl(null, [Validators.required])
  });

  constructor(
    private toast: ToastrService,
    private dataAPi: DataApiService,
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const change in changes) {
      if (changes.hasOwnProperty(change) && change !== 'reset') {
        this.formReview.get(change).setValue(changes[change].currentValue);
      } else if (change === 'reset') {
        this.getReset();
      }
    }
  }

  setValue(event, control: AbstractControl) {
    control.setValue(event.value);
  }

  getReset() {
    document.getElementById('close').click();
    this.reset = true;
    setTimeout(() => {
      this.reset = false;
    }, 2000);
  }

  submit() {
    if (this.formReview.valid) {
      if (this.tc) {
        const form = new FormData();
        for (const element in this.formReview.value) {
          if (this.formReview.value.hasOwnProperty(element)) {
            form.append(element, this.formReview.value[element]);
          }
        }

        this.dataAPi.sendReview(form).subscribe((data) => {
          if (data.result) {
            this.toast.success('Success', data.msg);
            this.getReset();
          } else {
            this.toast.error('Error', data.msg);
          }
        }, err => {
          this.toast.error('Error', 'Erro Connection');
        });

      } else {
        this.toast.error('Error', 'You must check in Term and Condition');
      }
      console.log(this.formReview.value);
    } else {
      this.toast.error('Error', 'You must fill in all fields');
    }
  }



}
