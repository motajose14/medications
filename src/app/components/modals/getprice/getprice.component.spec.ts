import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetpriceComponent } from './getprice.component';

describe('GetpriceComponent', () => {
  let component: GetpriceComponent;
  let fixture: ComponentFixture<GetpriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetpriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetpriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
