import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificatePdfComponent } from './certificate-pdf.component';

describe('CertificatePdfComponent', () => {
  let component: CertificatePdfComponent;
  let fixture: ComponentFixture<CertificatePdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificatePdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificatePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
