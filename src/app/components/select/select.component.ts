import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit, OnChanges {

  display = false;
  @Input() options: any[] = [];
  @Input() label = 'label';
  @Input() brand = false;
  @Input() labelValue = 'value';
  @Input() value = '30';
  @Input() input = false;
  @Input() inputType = 'text';
  @Output() changed = new EventEmitter<any>();
  customValue: any;
  name = '';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('options') ) {
      if (changes.options.firstChange ) {
        const options = changes.options.currentValue;
        this.name = options[0][this.label];
        if (changes.label.currentValue === 'commonname') {
          this.name = options[0][this.label] + '(' +  options[0].genericbrand + ')';
        }
      }
    }
  }

  openToggle(sOptions) {
    this.hideAllOptions();
    if (!this.display) {
      sOptions.style.display = 'block';
    }
    this.display = !this.display;
  }

  hideAllOptions() {
    const allOptions: any = document.getElementsByClassName('selectOptions');

    for (const item of allOptions) {
      item.style.display = 'none';
    }
  }

  setValue(option) {
    this.hideAllOptions();
    this.display = false;
    this.value = option[this.labelValue];
    if (this.label === 'commonname') {
      this.name = option[this.label] + '(' +  option.genericbrand + ')';
    } else {
      this.name = option[this.label];
    }
    this.sendChanged();
  }

  setCustomValue() {
    this.hideAllOptions();
    this.display = false;
    this.name = this.customValue;
    this.changed.emit({label: this.label, value: this.customValue});
  }

  sendChanged() {
    this.changed.emit({label: this.label, value: this.value});
  }

}
