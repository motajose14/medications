import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../services/dataApi/data-api.service';

@Component({
  selector: 'app-form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.css']
})
export class FormSearchComponent implements OnInit {

  formSearchR= new FormGroup({
    search:new FormControl(''),
    autocomplete: new FormControl('')
  });

  latitude:any=39.19550419999999;
  longitude:any=-76.7228227;
  @ViewChild('addresstext',{static:false}) addresstext:any;
  value:string;
  keyCode = 0;
  event = null;

  constructor(
    public mapsAPILoader: MapsAPILoader,
    public router: Router,
    public aRouter: ActivatedRoute,
    private dataApi: DataApiService
  ) { }

  ngOnInit() {
    this.getPlaceAutocomplete();
  }

  private getPlaceAutocomplete() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
          {
              componentRestrictions: { country: 'US' },
              types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          if(place.formatted_address!=undefined){
            //this.invokeEvent(res2);
            this.formSearchR.controls['autocomplete'].setValue(this.addresstext.nativeElement.value);

          }else{
            this.formSearchR.controls['autocomplete'].setValue(this.addresstext.nativeElement.value);
          }
        });
        this.searchPosition();
    });
  }

  searchPosition(){
    if ("geolocation" in navigator){ //check geolocation available
      //try to get user current location using getCurrentPosition() method
      navigator.geolocation.getCurrentPosition((position)=>{
        //console.log("Found your location nLat : "+position.coords.latitude+" nLang :"+ position.coords.longitude);
        this.latitude=position.coords.latitude;
        this.longitude=position.coords.longitude;
        this.changePosition();

      });
    }else{
        console.log("Browser doesn't support geolocation!");
    }
    this.changePosition();

  }

  changePosition(){
    this.mapsAPILoader.load().then(() => {
      if (navigator.geolocation) {
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(this.latitude,this.longitude);
        let request = { location: latlng };
        geocoder.geocode(request, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            let result = results[0];
            //let rsltAdrComponent = result.address_components;
            //let resultLength = rsltAdrComponent.length; result.address_components[2].long_name
            if (result != null) {
              //this.address = rsltAdrComponent[resultLength - 8].short_name;
              this.addresstext.nativeElement.value=result.address_components[2].long_name+" "+result.address_components[5].short_name+", "+result.address_components[6].short_name;
              this.formSearchR.get('autocomplete').setValue(this.addresstext.nativeElement.value);
            }
          }
        });
    }
    });
  }

  getSearch(){
    if(this.formSearchR.get('search').value!="" || this.formSearchR.get('autocomplete').value!=""){
      var drug_name="N/A";
      var location="N/A";
      this.dataApi.setApiDrug(this.event);
      if(this.formSearchR.get('search').value!="" && this.formSearchR.get('search').value!=null){
        drug_name=this.formSearchR.get('search').value;
      }
      if(this.formSearchR.get('autocomplete').value!="" && this.formSearchR.get('autocomplete').value!=null){
        location=this.formSearchR.get('autocomplete').value;
      }
    }
    this.router.navigate(["results",btoa(this.dataApi.apidrug.id),btoa(location)]);

  }

  setSearch(event){
    this.event = event;
    this.formSearchR.get('search').setValue(event.label);
  }

  setValue() {
    this.value = this.formSearchR.get('search').value;
  }
  keyDown(event) {
    if (event.keyCode === 40) {
      this.keyCode = event.keyCode;
    } else if ( event.keyCode === 38) {
      this.keyCode = event.keyCode;
    } else if (event.keyCode === 13) {
      if (this.event === null) {
        event.preventDefault();
        this.keyCode = event.keyCode;
      }
    }else {
      this.event = null;
    }

    setTimeout(() => {
      this.keyCode = -1;
    }, 100);
  }
}
