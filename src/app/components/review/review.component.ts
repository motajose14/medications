import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit, OnChanges {

  @Input() label = 'Example';
  @Input() valor = '0';
  @Input() types = false;
  @Input() disabled = false;
  @Input() reset = false;
  @Output() value: EventEmitter<any> = new EventEmitter<any>();
  id = Math.round(Math.random() * 100);
  time = null;

  constructor() { }

  ngOnInit() {
    if (this.types && this.valor === '0' && this.label === 'Overall, I have been satisfied with my experience') {
      this.valor = 'Yes';
    } else if (this.types && this.valor === '0' && (this.label === 'Condition' || this.label === 'Comments')) {
      this.valor = '';
    } else if (this.types && this.valor === '0' && (this.label === 'Treatment length')) {
      this.valor = 'Less than 1 month';
    } else if (this.types && this.valor === '0' && (this.label === 'Did your insurance cover this drug?')) {
      this.valor = 'Yes';
    } else if (this.types && this.valor === '0' && (this.label === 'Strength purchased' || this.label === 'Quantity purchased' || this.label === 'Approximate out of pocket monthly cost')) {
      this.valor = '';
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('reset')) {
      if (!changes.reset.firstChange) {
        if (changes.reset.currentValue) {
          if (this.label === 'Overall, I have been satisfied with my experience') {
            this.valor = 'Yes';
          } else if (this.label === 'Condition' || this.label === 'Comments') {
            this.valor = '';
          } else if (this.label === 'Treatment length') {
            this.valor = 'Less than 1 month';
          } else if (this.label === 'Did your insurance cover this drug?') {
            this.valor = 'Yes';
          } else if (this.label === 'Strength purchased' || this.label === 'Quantity purchased' || this.label === 'Approximate out of pocket monthly cost') {
            this.valor = '';
          } else {
            this.valor = '0';
            document.getElementById('star1' + this.id).removeAttribute('checked');
            document.getElementById('star2' + this.id).removeAttribute('checked');
            document.getElementById('star3' + this.id).removeAttribute('checked');
            document.getElementById('star4' + this.id).removeAttribute('checked');
            document.getElementById('star5' + this.id).removeAttribute('checked');
          }
        }
      }
    }
  }

  setValue() {
    clearTimeout(this.time);
    this.time = setTimeout(() => {
      this.value.emit({
        value: this.valor
      });
    }, 1000);
  }

}
