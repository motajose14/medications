import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  otpByMobile:boolean=false;
  sendOtp:boolean=false;
  sendOtpView:boolean=false;
  formRegister:FormGroup;

  constructor(
    public fb: FormBuilder,
    private toast: ToastrService,
    public dataApi:DataApiService,
    public router: Router
  ) { }

  ngOnInit() {
    this.formRegister=this.fb.group({
      first_name:[null,[Validators.required,Validators.minLength(3),Validators.maxLength(50),Validators.pattern(/^[a-zA-Z ]*$/)]],
      last_name:[null,[Validators.required,Validators.minLength(3),Validators.maxLength(50),Validators.pattern(/^[a-zA-Z ]*$/)]],
      username:[null,[Validators.required,Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})|[0-9]{9,10}$/)]],
      password:[null,[Validators.required,Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]],
      repassword:[null,[Validators.required,Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]],
      terms:[null,[this.validateTerm.bind(this)]]
    },{
      validators: this.validationPass,
    });
  }

  validationPass(control: AbstractControl){
    if(control.get('repassword').value !==control.get('password').value){
     control.get('repassword').setErrors({repass: true});
    }
  }

  validateTerm(control: AbstractControl){
    var res=null;
    if(!control.value){
      res={required:true};
    }
    return res;
  }

  otpBy(value:boolean){
    this.otpByMobile=value;
  }

  sendRegister(){
    if(this.formRegister.valid){
      var formData=new FormData();
      for(let element in this.formRegister.value){
        if(element!='repassword' && element!='terms'){
          formData.append(element,this.formRegister.controls[element].value);
        }
      } 
      this.dataApi.saveNewUser(formData).subscribe((data)=>{
        if(data.result){
          this.formRegister.reset();
          this.toast.success('Success',data.msg);
          this.router.navigate(['login']);
        }else{
          if(data.option==1){
            this.toast.info('Info',data.msg);
          }else{
            this.toast.error('Error',data.msg);
          }
        }
      },err=>{
        this.toast.error('Error','Error sending form');
      })
    }else{
      this.toast.error('Error','You must fill in all fields.');
    }
  }

}
