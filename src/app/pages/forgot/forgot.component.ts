import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  formForgot = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})|[0-9]{9,10}$/)])
  });

  constructor(
    private toastServices: ToastrService,
    private dataApi: DataApiService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }


  forgot() {
    if (this.formForgot.valid) {
      const form =  new FormData();
      form.append('email', this.formForgot.get('email').value);

      this.dataApi.forgotPassword(form).subscribe( (data) => {
        if (data.result) {
          this.toastServices.success('Success', data.msg);
          this.router.navigate(['/login']);
        } else {
          this.toastServices.error('Error', data.msg);
        }
      });

    } else {
      this.toastServices.error('Error', 'You must fill in all fields.');
    }
  }

}

