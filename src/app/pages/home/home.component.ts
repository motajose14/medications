import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { Router } from '@angular/router';
import { DataApiService } from '../../services/dataApi/data-api.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formSearch = new FormGroup({
    search: new FormControl(''),
    autocomplete: new FormControl(''),
    state: new FormControl('MD')
  });
  latitude: any = 39.19550419999999;
  longitude: any = -76.7228227;
  @ViewChild('addresstext', {static: false}) addresstext: any;
  options = [
    { value : 'Test', label : 'Test'},
    { value : 'Test 1', label : 'Test 1'},
    { value : 'Test 2', label : 'Test 2'}
  ];

  value: string;
  keyCode = 0;
  event = null;
  event2 = null;

  constructor(
    public mapsAPILoader: MapsAPILoader,
    public router: Router,
    private dataApi: DataApiService
  ) { }

  ngOnInit() {
    this.getPlaceAutocomplete();
  }

  getSearch() {
    let drug_name = '';
    let location = '';
    let state = '';
    this.dataApi.setApiDrug(this.event);
    setTimeout(() => {
      if (this.formSearch.get('search').value !== '' || this.formSearch.get('autocomplete').value !== '') {
        drug_name = 'N/A';
        location = 'N/A';
        state = 'N/A';
        if (this.formSearch.get('search').value !== '' && this.formSearch.get('search').value !== null) {
          drug_name = this.formSearch.get('search').value;
        }
        if (this.formSearch.get('autocomplete').value !== '' && this.formSearch.get('autocomplete').value !== null) {
          location = this.formSearch.get('autocomplete').value;
          state = this.formSearch.get('state').value;
        }
      }
      this.router.navigate(['results', btoa(this.dataApi.apidrug.id), btoa(location), btoa(state)]);
    }, 200);
  }

  private getPlaceAutocomplete() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
          {
              componentRestrictions: { country: 'US' },
              types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
  
          if (place.formatted_address !== undefined) {
            const sub = place.adr_address.substring(place.adr_address.indexOf('region') + 8);
            this.addresstext.nativeElement.value = place.formatted_address;
            this.formSearch.controls['autocomplete'].setValue(place.formatted_address);
            this.formSearch.get('state').setValue(sub.substring(0, sub.indexOf('</span')));
          } else {
            this.formSearch.controls['autocomplete'].setValue(this.addresstext.nativeElement.value);
          }
        });
        this.searchPosition();
    });
  }

  searchPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => { 
        this.latitude=position.coords.latitude;
        this.longitude=position.coords.longitude;
        this.changePosition();

      });
    } else {
        console.log('Browser doesn´t support geolocation!');
    }
    this.changePosition();
  }

  changePosition() {
    this.mapsAPILoader.load().then(() => {
      if (navigator.geolocation) {
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(this.latitude,this.longitude);
        let request = { location: latlng };
        geocoder.geocode(request, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            let result = results[0];
            if (result != null) {
              this.addresstext.nativeElement.value=result.address_components[2].long_name+' '+result.address_components[5].short_name+', '+result.address_components[6].short_name;
              this.formSearch.get('autocomplete').setValue(this.addresstext.nativeElement.value);
            }
          }
        });
    }
    });
  }

  setSearch(event) {
    this.event = event;
    this.formSearch.get('search').setValue(event.label);
  }

  setValue() {
    this.value = this.formSearch.get('search').value;
  }

  keyDown(event) {
    if (event.keyCode === 40) {
      this.keyCode = event.keyCode;
    } else if ( event.keyCode === 38) {
      this.keyCode = event.keyCode;
    } else if (event.keyCode === 13) {
      if (this.event === null) {
        event.preventDefault();
        this.keyCode = event.keyCode;
      }
    } else if (event.keyCode !== 9) {
      this.event = null;
    }

    setTimeout(() => {
      this.keyCode = -1;
    }, 100);
  }

  keyDown2(event) {
    if (event.keyCode === 13) {
      if (this.event2 === null || this.event2 !== this.formSearch.get('autocomplete').value) {
        event.preventDefault();
      }
      setTimeout(() => {
        this.event2 = this.formSearch.get('autocomplete').value;
      }, 500);

    }
  }

}
