import { Component, OnInit } from '@angular/core';
import { IMyWishlist, IShopping } from '../../interfaces/interfaces';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';

@Component({
  selector: 'app-whislist',
  templateUrl: './whislist.component.html',
  styleUrls: ['./whislist.component.css']
})
export class WhislistComponent implements OnInit {

  myWishlist: IMyWishlist[] = [];
  idUser: any;

  constructor(
    public dataApi: DataApiService,
    public toast: ToastrService,
    private router: Router,
    private shoppingServices: ShoppingCartService
  ) { }

  ngOnInit() {
    const datas = JSON.parse(localStorage.getItem('data'));
    this.idUser = datas.row;
    this.getMyWishlist();

  }

  getMyWishlist() {
    const form = new FormData();
    form.append('uid', this.idUser);
    this.dataApi.myWishlist(form).subscribe((data) => {
      this.myWishlist = data;
    });
  }

  removeMyWishlist(id,index) {
    const form = new FormData();
    form.append('id', id);
    this.dataApi.removeWishlist(form).subscribe((data) => {
      if (data.result) {
        this.toast.success('Success', data.msg);
        this.myWishlist.splice(index, 1);
      } else {
        this.toast.error('Error',data.msg);
      }
    });
  }

  addCart(item: IMyWishlist) {
    const product: IShopping  = {
      id: (item.brand !== null && item.brand !== '' ? item.brand : item.med_id),
      drugname: item.drug_name,
      qty: 1,
      price: Number(item.price),
      pharmacy_name: item.pharmacy_name,
      pharmacy_address: item.pharmacy_address,
      brand: item.brand,
      dosage: item.dosage,
      strength: item.strength,
      quantity: item.quantity,
      pharmacy_id: item.pharmacy_id
    };
    this.shoppingServices.setItem(product);
    this.router.navigate(['pay']);
  }

}
