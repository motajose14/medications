import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { passwordMatch } from '../../directive/password-match.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  formReset =  new FormGroup({
    password: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(50), Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]),
    repassword: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(50), Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)])
  }, {
    validators: passwordMatch
  });

  token = null;
  id = null;

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private dataApi: DataApiService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.activatedRouter.params.subscribe( (param) => {
      this.token = param.token;
      this.checkToken();
    });
  }

  checkToken() {
    const form =  new FormData();
    form.append('token', this.token);
    this.dataApi.checkToken(form).subscribe((data) => {
      if (!data.result) {
        this.toast.error('Error', data.msg);
        this.router.navigate(['/forgot']);
      }
    });
  }

  reset() {
    if (this.formReset.valid) {
      const form = new FormData();
      form.append('token', this.token);
      form.append('password', this.formReset.get('password').value);
      this.dataApi.resetPassword(form).subscribe( (data) => {
        if (data.result) {
          this.toast.success('Success', data.msg);
          this.router.navigate(['/login']);
        } else {
          this.toast.error('Error', data.msg);
        }
      });
    } else {
      this.toast.error('Error', 'You must fill in all fields.');
    }
  }

}
