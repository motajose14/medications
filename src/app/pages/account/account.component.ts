import { IDataUser, IUserAddress, IUserPayMethods } from './../../interfaces/interfaces';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { initFunctions } from '../../../../externo/js/custom_function';
import Swal from 'sweetalert2';
import { StripeCardComponent, ElementOptions, ElementsOptions } from 'ngx-stripe';
import { StripeService } from 'src/app/services/stripe/stripe.service';
import { IUCoins, IMyWishlist } from '../../interfaces/interfaces';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  idUser = '';
  userData: IDataUser;
  userDataOld: IDataUser;
  address = false;
  pay_methods = false;
  formAccount: FormGroup;
  formProfile: FormGroup;
  myCoins: IUCoins;
  @ViewChild(StripeCardComponent, {static: false}) card: StripeCardComponent;

  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }

  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  stripeTest: FormGroup;

  constructor(
    public dataApi: DataApiService,
    public toast: ToastrService,
    public fb: FormBuilder,
    private stripeService: StripeService
  ) { }

  ngOnInit() {
    this.formAccount = this.fb.group({
      first_name: [null, [Validators.required,Validators.minLength(3),Validators.maxLength(50),Validators.pattern(/^[a-zA-Z ]*$/)]],
      last_name: [null, [Validators.required,Validators.minLength(3),Validators.maxLength(50),Validators.pattern(/^[a-zA-Z ]*$/)]],
      email:[null,[Validators.required,Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      mobile:[null,[Validators.required,Validators.minLength(9),Validators.maxLength(9),Validators.pattern(/^[0-9]{9}$/)]],
      current_password:[null,[Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]],
      password:[null,[Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]],
      repassword:[null,[Validators.minLength(9),Validators.maxLength(50),Validators.pattern(/^[A-Za-z0-9_-]{6,50}$/)]],
    },{
      validators: this.validationPass,
    });
    this.formProfile=this.fb.group({
      gender:['Male',[]],
      birth_day:[null,[Validators.required]],
      type:['Home',[]],
      location:[null,[]]
    });
    this.getDataUser();
    this.getCoins();

    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
  }

  getDataUser(){
    var datas=JSON.parse(localStorage.getItem('data'));
    this.idUser=datas.row;
    var form=new FormData();
    form.append('row',this.idUser);
    this.dataApi.getDataUser(form).subscribe((data) => {
      this.userData = data;
      this.userDataOld = data;
      this.formAccount.controls['first_name'].setValue(this.userData.info.first_name);
      this.formAccount.controls['last_name'].setValue(this.userData.info.last_name);
      this.formAccount.controls['email'].setValue(this.userData.info.email);
      this.formAccount.controls['mobile'].setValue(this.userData.info.mobile);
      this.formProfile.controls['gender'].setValue((this.userData.info.gender!=null?this.userData.info.gender:'Male'));
      this.formProfile.controls['type'].setValue((this.userData.info.type!=null?this.userData.info.type:'Home'));
      this.formProfile.controls['birth_day'].setValue(this.userData.info.birth_day);
      this.formProfile.controls['location'].setValue(this.userData.info.location);
      setTimeout(() => {
        initFunctions.initTooltip();
      }, 500);
      if (this.userData.address.length !== 0) {
        this.address = true;
      }
      if (this.userData.pay_methods.length !== 0) {
        this.pay_methods = true;
      }
    }, err => {
      console.log(err);
      this.toast.error('Error', 'Error receiving data');
    });
  }

  getCoins() {
    const form = new FormData();
    form.append('uid', this.idUser);
    this.dataApi.getCoins(form).subscribe((data) => {
      this.myCoins = data;
    });
  }

  

  validationPass(control: AbstractControl) {
    if (control.get('repassword').value !== control.get('password').value) {
     control.get('repassword').setErrors({repass: true});
    }
  }

  sendAccount(){
    if(this.formAccount.valid){
      var form = new FormData();
      form.append('id',this.idUser);
      for(const element in this.formAccount.value){
        form.append(element,this.formAccount.controls[element].value);
      }
      this.dataApi.saveAccount(form).subscribe((data)=>{
        if(data.result){
          this.toast.success('Success',data.msg);
          this.formAccount.reset();
          this.getDataUser();
        }else{
          this.toast.error('Error',data.msg);
        }
      },err=>{
        this.toast.error('Error','Error sending form');
      });
    }else{
      this.toast.error('Error','You must fill in all fields');
    }
  }

  sendProfile(){
    if(this.formProfile.valid){
      var form = new FormData();
      form.append('id',this.idUser);
      for(const element in this.formProfile.value){
        form.append(element,this.formProfile.controls[element].value);
      }
      this.dataApi.saveProfile(form).subscribe((data)=>{
        if(data.result){
          this.toast.success('Success',data.msg);
          this.formProfile.reset();
          this.getDataUser();
        }else{
          this.toast.error('Error',data.msg);
        }
      },err=>{
        this.toast.error('Error','Error sending form');
      });
    }else{
      this.toast.error('Error','You must fill in all fields');
    }

  }

  getResult(e){
    if(e.reload){
      this.getDataUser();
    }
  }

  changeStatus(addressData:IUserAddress,status){
    if(status==1){
      Swal.fire({
        title: 'Sure!',
        text: 'This will be the default address',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, continue',
        showCancelButton: true,
      }).then((result) => {
        if (result.value) {
         this.sendChangeStatus(addressData,status);
        }
      });
    }else{
      Swal.fire({
        title: 'Sure!',
        text: 'This address will be deleted',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, continue',
        showCancelButton: true,
      }).then((result) => {
        if (result.value) {
          this.sendChangeStatus(addressData,status);
        }
      });
    }
  }

  sendChangeStatus(addressData: IUserAddress, status) {
    var form= new FormData();
    form.append('status',status);
    form.append('id',addressData.id);
   this.dataApi.saveStatusAddress(form).subscribe((data)=>{
     if(data.result){
       this.toast.success('Success',data.msg);
       this.getDataUser();
     }else{
       this.toast.error('Error',data.msg);
     }
   },err=>{
     this.toast.error('Error','Error sending data');
   });
  }

  buy() {
    this.stripeService.createToken(this.card.getCard(),this.stripeTest.get('name').value).subscribe((data)=>{
      if (data.token) {
        this.card.element.clear();
        this.stripeTest.reset();
        // Use the token to create a charge or a customer
        // https://stripe.com/docs/charges
        var form= new FormData();
        form.append('id',this.idUser);
        form.append('data',JSON.stringify(data));
        form.append('data2', data.token.id);
        this.dataApi.savePayMethods(form).subscribe((result)=>{
          if(result.result){
            this.toast.success('Success',result.msg);
            this.getDataUser();
          }else{
            this.toast.error('Error',result.msg);
          }
        },error=>{
          this.toast.error('Error','Connection error');
        });
      } else if (data.error) {
        // Error creating the token
        console.error(data.error.message);
        this.toast.error('Error',data.error.message);
      }
    },error=>{
      console.log('Error',error);
      this.toast.error('Error','Connection error');
    });
  }

  deleteCard(card:IUserPayMethods){
    console.log(card);
    var form= new FormData();
    form.append('id',card.id);
    this.dataApi.deletePayMethods(form).subscribe((data)=>{
      if(data.result){
        this.toast.success('Success',data.msg);
        this.getDataUser();
      }else{
        this.toast.error('Error',data.msg);
      }
    },error=>{
      this.toast.error('Error','Connection error');

    });
  }


}
