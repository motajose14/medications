import { Component, OnInit } from '@angular/core';
import { PrintServicesService } from 'src/app/services/print/print-services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  formPrint: FormGroup;

  constructor(
    private printServices: PrintServicesService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formPrint= this.fb.group({
      name:['',[Validators.required]],
      dateOfBird:['',[Validators.required]],
      telephone:['',[Validators.required]],
    });
  }

  printDoc(){
    if(this.formPrint.valid){
      this.printServices.printDocument(btoa(JSON.stringify(this.formPrint.value)));
    }
  }

}
