import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { initFunctions } from '../../../../externo/js/custom_function';
import { MapsAPILoader } from '@agm/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IsLoginService } from 'src/app/services/isLogin/is-login.service';
import { ShoppingCartService } from 'src/app/services/shopping-cart/shopping-cart.service';
import Swal from 'sweetalert2';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { isString, isObject } from 'util';
import { IDrugInfo, IQuantity, IShopping, IMrpPricingInfo, IMyWishlist } from '../../interfaces/interfaces';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

declare function init_bootstrap();

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnDestroy, AfterViewInit {

  selectBox = '';
  latitude: any = 39.19550419999999;
  longitude: any = -76.7228227;
  formSearchR = new FormGroup({
    search: new FormControl(''),
    autocomplete: new FormControl(''),
    state: new FormControl('')
  });

  cretificates: any;
  @ViewChild('addresstext', { static: false}) addresstext: any;
  brands = '';
  dosage = '';
  strength = '';
  qty = '';
  drugname: any;
  value: string;
  results: IDrugInfo[] = [];
  msg = 'Results not found';
  keyCode = 0;
  event = null;
  event2 = null;
  dateFrom = '';
  dateTo = '';
  distance = '';
  phtype = '';
  strengthV = '';
  dosageV = '';
  did = '';
  idUser: any;
  myWishlist: IMyWishlist[] = [];


  constructor(
    public mapsAPILoader: MapsAPILoader,
    public router: Router,
    public aRouter: ActivatedRoute,
    public isLogginServices: IsLoginService,
    private shoppingServices: ShoppingCartService,
    private dataApi: DataApiService,
    private toast: ToastrService,
  ) { }

  ngOnInit() {
    const datas = JSON.parse(localStorage.getItem('data')); 
    if (datas !== null && datas !== undefined) {
      this.idUser= datas.row;
      this.getMyWishlist();
    }
    this.selectBox = initFunctions.selectBox();
    initFunctions.initTooltip();
    this.getPlaceAutocomplete();
    this.aRouter.params.subscribe((params) => {
      if (params.location) {
        this.formSearchR.get('autocomplete').setValue(atob(params.location));
        setTimeout(() => {
          this.addresstext.nativeElement.value = atob(params.location);
        }, 1000);
      }

      if (params.state) {
        this.formSearchR.get('state').setValue(atob(params.state));
      }

      if (String(this.dataApi.apidrug.id) === atob(params.drug_name) ) {
        this.drugname = this.dataApi.apidrug.label;
        this.getDrug();
      }
    });

  }

  ngAfterViewInit() {


  }

  ngOnDestroy() {
  }

  getFilter() {
    for (const dis of this.dataApi.distances) {
      if (dis.checked) {
        this.distance = String(dis.distance);
      }
    }

    for (const type of this.dataApi.pharmacyTypes ) {
      if (type.checked) {
        this.phtype = type.title;
      }
    }

    this.dateFrom = this.dataApi.dateFrom;
    this.dateTo = this.dataApi.dateTo;

  }


  private getPlaceAutocomplete() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
          {
              componentRestrictions: { country: 'US' },
              types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          if (place.formatted_address !== undefined) {
            // this.invokeEvent(res2);
            this.formSearchR.controls['autocomplete'].setValue(place.formatted_address);
          } else {
            this.formSearchR.controls['autocomplete'].setValue(this.addresstext.nativeElement.value);
          }
        });

      this.searchPosition();

    });
  }

  searchPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.changePosition();

      });
    } else {
        console.log('Browser doesn´t support geolocation!');
    }
    this.changePosition();
  }

  changePosition() {
    this.mapsAPILoader.load().then(() => {
      if (navigator.geolocation) {
        const geocoder = new google.maps.Geocoder();
        const latlng = new google.maps.LatLng(this.latitude, this.longitude);
        const request = { location: latlng };
        geocoder.geocode(request, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            const result = results[0];
            if (result != null) {
              this.addresstext.nativeElement.value = result.address_components[2].long_name + ' ' + result.address_components[5].short_name + ', ' + result.address_components[6].short_name;
              this.formSearchR.get('autocomplete').setValue(this.addresstext.nativeElement.value);
            }
          }
        });
    }
    });
  }

  getFree() {
    this.router.navigate(['details', btoa('1')]);
  }

  buyCoupon(item: IMrpPricingInfo[]) {
    const product: IShopping  = {
      id: this.did,
      drugname: this.results[0].generalinfo[0].commonname,
      qty: 1,
      price: Number(item[1]),
      pharmacy_name: item[0].cname,
      pharmacy_address: item[0].caddress + ' ' + item[0].ccity + ' ' + item[0].cstate,
      brand: this.brands,
      dosage: this.dosage,
      strength: this.strength,
      quantity: this.qty,
      pharmacy_id: item[0].cid
    };
    this.shoppingServices.setItem(product);
    this.router.navigate(['pay']);
  }

  addWishlist(item: IMrpPricingInfo[], link: any) {
    const form = new FormData();
    form.append('drug_name', this.results[0].generalinfo[0].commonname);
    form.append('user_id', this.idUser);
    form.append('med_id', this.did);
    form.append('pharmacy_id', item[0].cid);
    form.append('pharmacy_name', item[0].cname);
    form.append('pharmacy_address', item[0].caddress + ' ' + item[0].ccity + ' ' + item[0].cstate);
    form.append('brand', this.brands);
    form.append('dosage', this.dosage);
    form.append('strength', this.strength);
    form.append('quantity', this.qty);
    form.append('price', String(item[1]));
    this.dataApi.addWishlist(form).subscribe( (data) => {
      this.toast.success('Success', 'Successfully saved');
      if (document.getElementById('mywishlist-' + link)) {
        document.getElementById('mywishlist-' + link).style.display = 'block';
      }

      if (document.getElementById('wishlist-' + link)) {
        document.getElementById('wishlist-' + link).style.display = 'none';
      }
      this.getMyWishlist();
    }, err => {
          this.toast.error('Error', 'Connection error');
    });
  }

  setSearch(event) {
    this.event = event;
    this.formSearchR.get('search').setValue(event.label);
  }

  getSearch() {
    let drug_name = '';
    let location = '';
    let state = '';
    this.dataApi.setApiDrug(this.event);
    setTimeout(() => {
      if (this.formSearchR.get('search').value !== '' || this.formSearchR.get('autocomplete').value !== '') {
        drug_name = 'N/A';
        location = 'N/A';
        state = 'N/A';
        if (this.formSearchR.get('search').value !== '' && this.formSearchR.get('search').value !== null) {
          drug_name = this.formSearchR.get('search').value;
          this.formSearchR.get('search').setValue(null);
        }
        if (this.formSearchR.get('autocomplete').value !== '' && this.formSearchR.get('autocomplete').value !== null) {
          location = this.formSearchR.get('autocomplete').value;
          state = this.formSearchR.get('state').value;
        }
      }
      this.router.navigate(['results', btoa(this.dataApi.apidrug.id), btoa(location), btoa(state)]);
    }, 200);
  }

  setValue(event) {
    if (event.keyCode !== 13) {
      this.value = this.formSearchR.get('search').value;
    }
  }

  openUrl(url) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are leaving the MyRxPal website and MyRxPal is not responsible for the content, accuracy or security of the website your are being directed by this link',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Proceed'
    }).then((result) => {
      if (result.value) {
        window.open(url, '_blank');
      }
    });
  }

  getDrug() {
    this.getFilter();
    /* const value = this.dataApi.apidrug.id + (this.formSearchR.get('state').value !== '' ? '/' + this.formSearchR.get('state').value : ''); */
    this.results = [];
    const form = new FormData();
    form.append('distance', this.distance);
    form.append('phtype', this.phtype);
    form.append('fromdate', this.dateFrom);
    form.append('todate', this.dateTo);
    this.dataApi.getDrug(this.dataApi.apidrug.id, form).subscribe((data) => {
     if (isObject(data)) {
       this.results = data as IDrugInfo[];
       setTimeout(() => {
        initFunctions.selectBox();
        setTimeout(() => {
          initFunctions.initTab();
          this.markWishlist();

        }, 500);
       }, 50);
       this.strength = data[0].defaultstrengths[0].strength;
       this.dosage = data[0].dosageform;
       this.qty = data[0].qty;
       this.did = data[0].did;
     } else if (isString(data)) {
       this.msg = data as string;
     } else {
       this.msg = 'Results not found';
     }
    }, err => {
      console.error('Error' , err);
    });
  }

  updateSearch() {
    let value = '';
    if (this.brands !== '' ) {
      value +=  this.brands;
      this.did = this.brands;
    } else {
      value +=  this.did;
    }
    value += (this.formSearchR.get('state').value !== '' ? '/' + this.formSearchR.get('state').value : '/md');

    if (this.dosage !== '') {
      value += '/' + encodeURIComponent(this.dosage);
      this.dosageV = this.dosage;

    }
    if (this.strength !== '') {
      value += '/' + encodeURIComponent(this.strength);
      this.strengthV = this.strength;
    }
    if (this.qty !== '' ) {
      value += '/' + this.qty;
    }

    this.getFilter();
    const form = new FormData();
    form.append('distance', this.distance);
    form.append('phtype', this.phtype);
    form.append('fromdate', this.dateFrom);
    form.append('todate', this.dateTo);

    this.dataApi.getDrug(value, form).pipe(
      map((data) => {
        if (isObject(data)) {
          data = data.map((item: IDrugInfo) => {
            if (item.hasOwnProperty('defaultstrengths')) {
              if (item.defaultstrengths.length === 0) {
                item.defaultstrengths = this.results[0].defaultstrengths;
              }
            } else {
              item.defaultstrengths = this.results[0].defaultstrengths;
            }

            if (item.hasOwnProperty('dosageforms')) {
              if (item.dosageforms.length === 0) {
                item.dosageforms = this.results[0].dosageforms;
              }
            } else {
              item.dosageforms = this.results[0].dosageforms;
            }

            if (item.hasOwnProperty('quantities')) {
              if (item.quantities !== null) {
                if (item.quantities.length === 0) {
                  item.quantities = this.results[0].quantities;
                }
              } else {
                  item.quantities = this.results[0].quantities;
              }
            } else {
              item.quantities = this.results[0].quantities;
            }

            if (item.hasOwnProperty('brandinfo')) {
              if (item.brandinfo.length === 0) {
                item.brandinfo = this.results[0].brandinfo;
              }
            } else {
              item.brandinfo = this.results[0].brandinfo;
            }

            return item;
          });
        }

        return data;
      })
    ).subscribe((data) => {
     this.results = [];
     if (isObject(data)) {
       this.results = data as IDrugInfo[];
       /* this.results = this.results.map( (result) => {
         result.quantities = result.quantities.map( (qtys) => {
           const qty = qtys.quantities.split(',');
           const resp: IQuantity[] = [];
           for (const q of qty) {
             resp.push({
              quantity: q
            });
           }
           qtys.quantity = resp;
           return qtys;
         });
         return result;
       } ); */
       setTimeout(() => {
        initFunctions.selectBox();
        initFunctions.initTab();
        this.markWishlist();
       }, 500);
     } else if (isString(data)) {
       this.msg = data as string;
     } else {
       this.msg = 'Results not found';
     }
    }, err => {
      console.error('Error' , err);
    });
  }

  getMyWishlist() {
    const form = new FormData();
    form.append('uid', this.idUser);
    this.dataApi.myWishlist(form).subscribe((data) => {
      this.myWishlist = data;
    });
  }

  removeMyWishlist(item, btn) {
    const id = document.getElementById('mywishlist-' + btn).getAttribute('data-id');
    const form = new FormData();
    form.append('id', id);
    this.dataApi.removeWishlist(form).subscribe((data) => {
      if (data.result) {
        this.toast.success('Success', data.msg);
        if (document.getElementById('mywishlist-' + btn)) {
          document.getElementById('mywishlist-' + btn).style.display = 'none';
        }

        if (document.getElementById('wishlist-' + btn)) {
          document.getElementById('wishlist-' + btn).style.display = 'block';
        }
        this.getMyWishlist();
        for (let i = 0; i < this.myWishlist.length; i++) {
          if (this.myWishlist[i].id === id) {
            this.myWishlist.splice(i,1);
            return;
          }
        }
      } else {
        this.toast.error('Error', data.msg);
      }
    });
  }

  markWishlist() {
    this.myWishlist.map( (item) => {
      const reg = new RegExp(' ', 'g');
      if (document.getElementById('mywishlist-' + item.med_id + '-' + item.pharmacy_name.replace(reg, '-') + '-' + item.dosage + '-' + item.strength + '-' + item.quantity)) {
        document.getElementById('mywishlist-' + item.med_id + '-' + item.pharmacy_name.replace(reg, '-') + '-' + item.dosage + '-' + item.strength + '-' + item.quantity).style.display = 'block';
        document.getElementById('mywishlist-' + item.med_id + '-' + item.pharmacy_name.replace(reg, '-') + '-' + item.dosage + '-' + item.strength + '-' + item.quantity).setAttribute('data-id', item.id);
      }
      if (document.getElementById('wishlist-' + item.med_id + '-' + item.pharmacy_name.replace(reg, '-') + '-' + item.dosage + '-' + item.strength + '-' + item.quantity)) {
        document.getElementById('wishlist-' + item.med_id + '-' + item.pharmacy_name.replace(reg, '-') + '-' + item.dosage + '-' + item.strength + '-' + item.quantity).style.display = 'none';
      }
    });
  }

  keyDown(event) {
    if (event.keyCode === 40) {
      this.keyCode = event.keyCode;
    } else if ( event.keyCode === 38) {
      this.keyCode = event.keyCode;
    } else if (event.keyCode === 13) {
      if (this.event === null) {
        event.preventDefault();
        this.keyCode = event.keyCode;
      }
    } else if (event.keyCode === 27) {
      this.keyCode = event.keyCode;
    } else if (event.keyCode !== 9) {
      this.event = null;
    }

    setTimeout(() => {
      this.keyCode = -1;
    }, 100);
  }

  keyDown2(event) {
    if (event.keyCode === 13) {
      if (this.event2 === null || this.event2 !== this.formSearchR.get('autocomplete').value) {
        event.preventDefault();
      }
      setTimeout(() => {
        this.event2 = this.formSearchR.get('autocomplete').value;
      }, 500);

    }
  }

  getChanged(event) {
    switch (event.label) {
      case 'quantity': this.qty = event.value; break;
      case 'strength': this.strength = event.value; break;
      case 'dosage': this.dosage = event.value; break;
      case 'commonname': this.brands = event.value; break;
      default: break;
    }
    this.updateSearch();
  }
  /* testShopping() {
    const product: IShopping  = {
      id: String(Math.random() * 10000),
      drugname: 'Test Name ' + Math.random() * 100,
      qty: 1,
      price: Math.random() * 100,
      pharmacy_name: 'test pharmacy',
      pharmacy_address: 'Test Location',
    };
    this.shoppingServices.setItem(product);
  } */

}
