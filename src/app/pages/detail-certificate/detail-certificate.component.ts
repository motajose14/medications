import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ICerts, IShopping } from '../../interfaces/interfaces';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';

@Component({
  selector: 'app-detail-certificate',
  templateUrl: './detail-certificate.component.html',
  styleUrls: ['./detail-certificate.component.css']
})
export class DetailCertificateComponent implements OnInit {

  howShow = true;
  certificate: ICerts[] = [];
  reset = false;
  orderdet_id = '';
  user_id = '';

  constructor(
    private router: Router,
    private dataApi: DataApiService,
    private shopping: ShoppingCartService,
    private aRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const data = JSON.parse(localStorage.getItem('data'));
    this.user_id = data.row;
    this.aRoute.params.subscribe((param) => {
      this.getCert(param.id);

    });
  }

  buyCoupon(item: IShopping) {
    this.shopping.setItem(item);
    this.router.navigate(['pay']);
  }

  getCert(id) {
    const form = new FormData();
    form.append('id', atob(id));
    this.dataApi.getCerts(form).subscribe((data) => {
      this.certificate = data;
      this.orderdet_id = this.certificate[0].orderdet_id;
    });
  }

  getHow(value: boolean) {
    this.howShow = value;
  }
  getReset() {
    this.reset = true;
    setTimeout(() => {
       this.reset = false;
     }, 500);
  }

}
