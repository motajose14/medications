import { Component, OnInit } from '@angular/core';
import { PrintServicesService } from 'src/app/services/print/print-services.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {

  data:any;

  constructor(
    private printServices: PrintServicesService,
    private aRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.aRoute.params.subscribe((params)=>{
      this.data=JSON.parse(atob(params.data));
    });
  }

  ngAfterViewInit(){
    setTimeout(() => {
      window.print();
    }, 50);
    setTimeout(() => {
      this.printServices.onDataReady();
    }, 100);
    
  }

}
