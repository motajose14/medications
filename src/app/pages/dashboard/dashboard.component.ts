import { Component, OnInit, OnDestroy } from '@angular/core';
import { initFunctions } from '../../../../externo/js/custom_function';
import { Router } from '@angular/router';
import { DataApiService } from '../../services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { ICerts, IShopping } from '../../interfaces/interfaces';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  certificates: ICerts[] = [];

  certificatesShow: ICerts[] = [];
  select = 0;
  idUser = '';
  orderdet_id = '';

  reset = false;

  constructor(
    private router: Router,
    private dataApi: DataApiService,
    private toast: ToastrService,
    private shoppingService: ShoppingCartService
  ) { }

  ngOnInit() {
    const data = JSON.parse(localStorage.getItem('data'));
    this.idUser = data.row;
    this.getMyCerts();
  }

  ngOnDestroy() {
  }

  getCertificate(id) {
    this.router.navigate(['detail/certificate', btoa(id)]);
  }

  filterCertificate() {
    const value = String(this.select);
    this.certificatesShow = this.certificates;
    if (value !== '0') {
      this.certificatesShow = this.certificatesShow.filter((certificate) => {
        return certificate.status === value ? certificate : null;
      });
    }
  }

  getMyCerts() {
    const form = new FormData();
    form.append('id', this.idUser);

    this.dataApi.getMyCerts(form).subscribe((data) => {
      this.certificates = data;
      this.filterCertificate();

    }, err => {
      this.toast.error('Error', 'Error Connection');
    });
  }

  buyCoupon(item: IShopping) {
    this.shoppingService.setItem(item);
    this.router.navigate(['pay']);
  }

  getReset(id) {
    this.reset = true;
    this.orderdet_id = id;
    setTimeout(() => {
      this.reset = false;
    }, 500);
  }

}
