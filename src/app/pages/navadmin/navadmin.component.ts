import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IsLoginService } from 'src/app/services/isLogin/is-login.service';

@Component({
  selector: 'app-navadmin',
  templateUrl: './navadmin.component.html',
  styleUrls: ['./navadmin.component.css']
})
export class NavadminComponent implements OnInit {

  constructor(
    private router:Router,
    private isLogginServices: IsLoginService
  ) { }

  ngOnInit() {
  }

  logout(){
    localStorage.setItem('loggin','false');
    this.isLogginServices.setLoggin(false);
    this.router.navigate(['/home']);
  }

}
