import { IDataUser } from 'src/app/interfaces/interfaces';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart/shopping-cart.service';
import { initFunctions } from '../../../../externo/js/custom_function';
import { StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StripeService } from 'src/app/services/stripe/stripe.service';
import { DataApiService } from 'src/app/services/dataApi/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { IShopping, IUCoins } from '../../interfaces/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  items: IShopping[] = [];
  amount = 0;
  token: any;
  myCoins: IUCoins = {
    balance: 0
  };

  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }

  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  @ViewChild(StripeCardComponent,{static:false}) card: StripeCardComponent;

  stripeTest: FormGroup;
  newPayment = '1';

  idUser = '';
  userData: IDataUser;
  pay_methods: boolean = false;

  constructor(
    private shoppingCartServices: ShoppingCartService,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private dataApi: DataApiService,
    private toast: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.items=this.shoppingCartServices.getItemsShoppingCarts();
    console.log(this.items);
    this.updateAmount();
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });

    this.getDataUser();
    this.getCoins();

  }

  getDataUser(){
    var datas=JSON.parse(localStorage.getItem('data'));
    this.idUser=datas.row;
    var form=new FormData();
    form.append('row',this.idUser);
    this.dataApi.getDataUser(form).subscribe((data)=>{
      this.userData=data;
      if(this.userData.pay_methods.length!=0){
        this.pay_methods=true;
      }
      setTimeout(() => {
        initFunctions.initTooltip();
      }, 500);
    },err=>{
      this.toast.error('Error', 'Error receiving data');
    });
  }

   getCoins() {
    const form = new FormData();
    form.append('uid', this.idUser);
    this.dataApi.getCoins(form).subscribe((data) => {
      this.myCoins = data;
    });
  }

  deleteItem(i) {
   this.items.splice(i, 1);
   this.shoppingCartServices.updateItems(this.items);
   this.updateAmount();
  }

  updateQty(i) {
    var value2=initFunctions.getValue("inputEmail"+i);
    this.items[i].qty=Number(value2);
    this.shoppingCartServices.updateItems(this.items);
    this.updateAmount();
  }

  updateAmount(){
    this.amount=0;
    for(let item of this.items){
      this.amount+=(item.price * item.qty);
    }
  }

  addCard() {
    this.stripeService.createToken(this.card.getCard(), this.stripeTest.get('name').value).subscribe((data)=>{
      if (data.token) {
        this.card.element.clear();
        this.stripeTest.reset();
        var form= new FormData();
        form.append('id',this.idUser);
        form.append('data2', data.token.id);
        this.dataApi.savePayMethods(form).subscribe((result)=>{
          if(result.result){
            this.toast.success('Success',result.msg);
            this.getDataUser();
            this.newPayment='1';
          }else{
            this.toast.error('Error',result.msg);
          }
        },error=>{
          this.toast.error('Error','Connection error');
        });
      } else if (data.error) {
        console.error(data.error.message);
        this.toast.error('Error',data.error.message);
      }
    },error=>{
      console.log('Error',error);
      this.toast.error('Error','Connection error');
    });
  }

  clickCard(pay_method) {
    this.token = pay_method.data;
  }

  placeOrder() {
    console.log(this.newPayment, this.token, this.myCoins.balance, this.amount);
    if ((this.newPayment === '1' && this.token !== undefined) || (this.newPayment === '3' && this.myCoins.balance >= this.amount)) {
      const form = new FormData();
      form.append('row', this.idUser);
      form.append('items', JSON.stringify(this.items));
      form.append('type', this.newPayment);
      if (this.newPayment === '1') {
        form.append('card', JSON.stringify(this.token));
      }
      this.dataApi.createCharge(form).subscribe((data) => {
        if (data.result) {
          this.toast.success('Success', data.msg);
          /* this.shoppingCartServices.clearItems(); */
          this.router.navigate(['']);
        } else {
          this.toast.error('Error', 'error saved the order');
        }
      }, err => { console.log(err); });
    } else {
      this.toast.error('Error', 'You must select a card');
    }
  }

}
