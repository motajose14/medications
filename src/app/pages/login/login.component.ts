import { DataApiService } from 'src/app/services/dataApi/data-api.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IsLoginService } from 'src/app/services/isLogin/is-login.service';
import { Router } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin:FormGroup;
  constructor(
    private fb: FormBuilder,
    private toastr:ToastrService,
    private isLogginService:IsLoginService,
    private router: Router,
    private dataApi:DataApiService
  ) { 
  }

  ngOnInit() {
    this.formLogin=this.fb.group({
      username:[null,[Validators.required]],
      password:[null,[Validators.required, Validators.minLength(6)]]
    });
  }

  getLogin() {
    if (this.formLogin.valid) {
      var formData=new FormData();
      for (const element in this.formLogin.value) {
        formData.append(element,this.formLogin.controls[element].value)
      }

      this.dataApi.login(formData).subscribe((data)=>{
        if(data.result){
          this.toastr.success('Welcome!', '');
          localStorage.setItem('loggin','true');
          localStorage.setItem('data',JSON.stringify(data.log));
          this.isLogginService.setLoggin(true);
          history.back();
        }else{
          this.toastr.error('Error',data.msg);
        }
      },err=>{
        this.toastr.error('Error','Error sending form');
      });
      /*this.toastr.success('Welcome!', '');
      localStorage.setItem('loggin','true');
      this.isLogginService.setLoggin(true);
      this.router.navigate(['/dashboard']);*/
    }else{

    }
  }

}
