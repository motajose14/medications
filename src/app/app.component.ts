import { Component } from '@angular/core';
import { PrintServicesService } from './services/print/print-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'medications';
  constructor(
    public printServices: PrintServicesService
  ){}
}
