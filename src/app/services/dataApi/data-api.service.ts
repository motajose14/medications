import { Injectable } from '@angular/core';
import { HttpClient,  HttpErrorResponse } from '@angular/common/http';
import {throwError as observableThrowError,   Observable } from 'rxjs';
import { tap,  catchError, retry } from 'rxjs/operators';
import { IUsers,  IResp,  IDataUser, IQuantity, IDrugInfo, IUCoins, IMyWishlist, ICerts } from 'src/app/interfaces/interfaces';
import { environment } from './../../../environments/environment';
import { isObject, isString } from 'util';

@Injectable({
  providedIn:  'root'
})
export class DataApiService {

  url_api = environment.url_api;
  url_api2 = environment.url_api2;
  public selectedDeal = {
    dealid: '', title: '', dealdesc: '', retailprice: '', dealprice: '', deallink: '', img1: '', img2: '',
    fineprint: '', startdate: '', enddate: '', views: '', likes: '',
    clicks: '', maincat: '', subcat: '', sub1cat: '', sellername: '', caddress: '', ccity: '', cstate: '', ctel: ''};
  public distances = [
    {id: 1,  title: '1 mile',  distance: 1, checked: false},
    {id: 2,  title: '5 miles',  distance: 5, checked: true},
    {id: 3,  title: '10 miles',  distance: 10, checked: false},
    {id: 4,  title: '25 miles',  distance: 25, checked: false},
    {id: 5,  title: '50 miles',  distance: 50, checked: false}
  ];

  /* public sellerRating = [
    {id: 1,  title: 'Any',  value: 'any', checked: true},
    {id: 2,  title: '1 Star',  value: 1, checked: false},
    {id: 3,  title: '2 Star',  value: 2, checked: false},
    {id: 4,  title: '3 Star',  value: 3, checked: false},
    {id: 5,  title: '4 Star',  value: 4, checked: false},
    {id: 6,  title: '5 Star',  value: 5, checked: false}
  ]; */

  public pharmacyTypes = [
    {id: 10,  title:  'Any',  color:  '#F3F781', checked: true },
    {id: 11,  title:  'Assisted Living',  color:  '#F3F781', checked: false },
    {id: 12,  title: 'Chain (10 or more stores)', color:   '#E1F5A9', checked: false },
    {id: 13,  title: 'Clinic',  color:  '#D0F5A9', checked: false },
    {id: 14,  title: 'Community (less than 10 stores)', color:   '#82FA58', checked: false },
    {id: 15,  title: 'Comprehensive Care (Long Term Care)', color:   '#58FA58', checked: false },
    {id: 16,  title: 'Consultant', color:   '#58FAAC', checked: false },
    {id: 17,  title: 'Correctional Institution', color:   '#00BFFF', checked: false },
    {id: 18,  title: 'Free Clinic', color:   '#0080FF', checked: false },
    {id: 19,  title: 'HMO', color:   '#BF00FF', checked: false },
    {id: 20,  title: 'Durable Medical Equipment (DME) / Device', color:   '#FE2EF7', checked: false },
    {id: 21,  title: 'Home Health', color:   '#FA8258', checked: false },
    {id: 22,  title: 'Hospital', color:   '#F8E0E6', checked: false },
    {id: 23,  title: 'Independent', color:   '#F9D1D1', checked: false },
    {id: 24,  title: 'Internet', color:   '#E6E0F8', checked: false },
    {id: 25,  title: 'Intravenous Therapy',  color:  '#688A08', checked: false },
    {id: 26,  title: 'Mail Order', color:   '#8258FA', checked: false },
    {id: 27,  title: 'Managed Care', color:   '#CED8F6', checked: false },
    {id: 28,  title: 'Nursing Home',  color:  '#F6CED8', checked: false },
    {id: 29,  title: 'Non Sterile Compounding', color:   '#610B4B', checked: false },
    {id: 30,  title: 'Nuclear', color:   '#01A9DB', checked: false },
    {id: 31,  title: 'Veterinary', color:   '#8A084B', checked: false },
    {id: 32,  title: 'Pharmacy Service Center', color:   '#CEECF5', checked: false },
    {id: 33,  title: 'Research',  color:  '#A4A4A4', checked: false },
    {id: 34,  title: 'Sterile Compounding', color:   '#FFBF00', checked: false}
  ];

  apidrug = {
   label: '',
   id: ''
  };

  public dateFrom: any = '';
  public dateTo: any = '';


  public pharmacyBadges = [];

  constructor(public http: HttpClient) {
    this.dateFrom = new Date();
    this.dateFrom = this.dateFrom.getFullYear() + '/' + (String(this.dateFrom.getMonth() + 1).length === 1 ? '0' + (this.dateFrom.getMonth() + 1) : this.dateFrom.getMonth() + 1) + '/' + this.dateFrom.getDate();
    this.dateTo = new Date();
    this.dateTo = this.dateTo.getFullYear() + '/' + (String(this.dateTo.getMonth() + 2).length === 1 ? '0' + (this.dateTo.getMonth() + 2) : this.dateTo.getMonth() + 2) + '/' + this.dateTo.getDate();
    this.getApiDrug();
   }

   reserDate() {
    this.dateFrom = new Date();
    this.dateFrom = this.dateFrom.getFullYear() + '/' + (String(this.dateFrom.getMonth() + 1).length === 1 ? '0' + (this.dateFrom.getMonth() + 1) : this.dateFrom.getMonth() + 1) + '/' + this.dateFrom.getDate();
    this.dateTo = new Date();
    this.dateTo = this.dateTo.getFullYear() + '/' + (String(this.dateTo.getMonth() + 2).length === 1 ? '0' + (this.dateTo.getMonth() + 2) : this.dateTo.getMonth() + 2) + '/' + this.dateTo.getDate();
   }

  getApiDrug() {
    this.apidrug = JSON.parse(localStorage.getItem('apidrug')) || {label: '', id: ''};
  }

  setApiDrug(apiDrug) {
    this.apidrug = apiDrug;
    localStorage.setItem('apidrug', JSON.stringify(this.apidrug));
  }

  saveNewUser(form): Observable<IResp> {
      return this.http.post<IResp>(this.url_api + '/saveuser?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  login(form): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/login?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getDataUser(form): Observable<IDataUser> {
    return this.http.post<IDataUser>(this.url_api + '/getdatauser?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
      data.pay_methods = data.pay_methods.map((paymethod) => {
        paymethod.data = JSON.parse(paymethod.data);
        return paymethod;
      });

      return data;

    }),  catchError(this.errorHandler));
  }

  saveAccount(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/saveaccount?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  saveProfile(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/saveprofile?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  saveAddress(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/saveaddress?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  saveStatusAddress(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/statusaddress?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  savePayMethods(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/savepaymethods?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getPayMethods(form: FormData) {
    return this.http.post(this.url_api + '/getpaymethods?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  deletePayMethods(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/deletepaymethods?t=' + new Date().getTime(), form )
    .pipe(tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getAutocomplete(value: string): Observable<any[]> {
    return this.http.get<any[]>(this.url_api2 + '/getdrugnames/' + value + '?t=' + new Date().getTime())
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getIp(): Observable<any> {
    return this.http.get<any>('https: //jsonip.com?t=' + new Date().getTime(), {responseType: 'json'} )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getDrug(value, form): Observable<IDrugInfo[] | any> {
    console.log(this.url_api2 + '/getdruginfonew/' + value);
    return this.http.post<IDrugInfo[] | any>(this.url_api2 + '/getdruginfonew/' + value + '?t=' + new Date().getTime(), form)
    .pipe(
      retry(2),
      tap((data) => {
      data = data.map((item) => {
        if (isString(item.quantities)) {
          const qty = item.quantities.split(',');
          const qtys: IQuantity[] = [];
          for (const q of qty) {
            qtys.push({
             quantity: q
           });
          }
          item.quantities = qtys;
        }
        const brands = (item.brandinfo === '' ? [] : item.brandinfo);
        const generic = item.genericinfo;
        if (isObject(generic)) {
          generic.map((g) => {
            brands.unshift(g);
          });
        }
        if (brands.hasOwnProperty('default_pricinginfo')) {
          item['default_pricinginfo'] = brands['default_pricinginfo'];
          delete brands.default_pricinginfo;
        }

        const generics = (item.generalinfo === '' ? [] : item.generalinfo);
        if (isObject(generics)) {
          generics.map((g) => {
            brands.unshift(g);
          });
        }

        item.brandinfo = brands;
        const reg = new RegExp(' ', 'g');
        item.mrp_pricinginfo = item.mrp_pricinginfo.map( (mrp) => {
          mrp[0].name = mrp[0].cname.replace(reg, '-');
          return mrp;
        });

        return item;
      });
      return data;
    }),  catchError(this.errorHandler));
  }

  updateSearchDrug(value): Observable<IDrugInfo[] | any> {
    return this.http.get<IDrugInfo[] | any>(this.url_api2 + '/getdruginfonew/' + value + '?t=' + new Date().getTime())
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  forgotPassword(form: FormData): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/forgot-password?t=' + new Date().getTime(), form )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  checkToken(fields): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/check-token?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  createCharge(fields): Observable<any> {
    return this.http.post<any>(this.url_api + '/create-charge?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  resetPassword(fields): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/reset-password?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getCoins(fields): Observable<IUCoins> {
    return this.http.post<IUCoins>(this.url_api + '/getcoins?t=' + new Date().getTime(), fields)
    .pipe(
      retry(2),
      tap((data) => {
        const mycoins = data;
        if (mycoins.coins.length === 0) {
          mycoins.balance = 0;
        } else {
          mycoins.balance = mycoins.coins[0].coins;
          mycoins.id = mycoins.coins[0].id;
          mycoins.tradeinson = mycoins.coins[0].tradeinson;
          mycoins.ucoins = [];
          const amts = mycoins.coins[0].amts.split(',');
          const orderids = mycoins.coins[0].orderids.split(',');
          let i = 0;

          for (const amt of amts) {
            mycoins.ucoins.push({
              amts: Number(amt),
              orderids: orderids[i],
              description: (Number(amt) < 0 ? 'Payment' : 'Refound')
            });
          }
        }
        return mycoins;
      }),  catchError(this.errorHandler)
    );
  }

   addWishlist(fields): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/addwishlist?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  removeWishlist(fields): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/removewishlist?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  myWishlist(fields): Observable<IMyWishlist[]> {
    return this.http.post<IMyWishlist[]>(this.url_api + '/mywishlist?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
    }),  catchError(this.errorHandler));
  }

  getMyCerts(fields): Observable<ICerts[]> {
    return this.http.post<ICerts[]>(this.url_api + '/mycerts?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
        return data.map((cert) => {
          cert.orderon = cert.orderon * 1000;
          cert.expiredon = cert.expiredon * 1000;
          cert.disputeon = cert.disputeon * 1000;
          cert.redeemon = cert.redeemon * 1000;
          cert.productinfo = JSON.parse(cert.product);
          return cert;
        });
    }),  catchError(this.errorHandler));
  }

  getCerts(fields): Observable<ICerts[]> {
    return this.http.post<ICerts[]>(this.url_api + '/mycert?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
        return data.map((cert) => {
          cert.orderon = cert.orderon * 1000;
          cert.expiredon = cert.expiredon * 1000;
          cert.disputeon = cert.disputeon * 1000;
          cert.redeemon = cert.redeemon * 1000;
          cert.productinfo = JSON.parse(cert.product);
          return cert;
        });
    }),  catchError(this.errorHandler));
  }

  sendReview(fields): Observable<IResp> {
    return this.http.post<IResp>(this.url_api + '/sendreview?t=' + new Date().getTime(), fields )
    .pipe(
      retry(2),
      tap((data) => {
        
    }),  catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Server error');
  }
}
