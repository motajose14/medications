import { Injectable } from '@angular/core';
import { IShopping } from '../../interfaces/interfaces';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  items: IShopping[] = [];

  constructor() {
    const localItems = localStorage.getItem('shopping-cart');
    this.items = JSON.parse((localItems == null ? '[]' : atob(localItems)));
  }

  itemsCart(): Observable<number> {
    return new Observable( (obeserver: Subscriber<number>) => {
      let num = 0;
      setInterval( () => {
        if (this.items.length !== num ) {
          num = this.items.length;
          obeserver.next(num);
        }
      }, 1500);
    });
  }

  getItemsShoppingCarts() {
    return this.items;
  }

  setItem(item: IShopping) {
    let check = false;
    this.items.map((i) => {
      if (item.id === i.id && item.pharmacy_id === i.pharmacy_id && item.dosage === i.dosage && item.strength === i.strength && item.quantity === i.quantity) {
        check = true;
        return true;
      }
    });
    if (!check) {
      this.items.push(item);
      localStorage.setItem('shopping-cart', btoa(JSON.stringify(this.items)));
    } else {
      this.items = this.items.map((i) => {
        if (item.id === i.id && item.pharmacy_id === i.pharmacy_id && item.dosage === i.dosage && item.strength === i.strength && item.quantity === i.quantity) {
          i = item;
        }
        return i;
      });
      localStorage.setItem('shopping-cart', btoa(JSON.stringify(this.items)));
    }
  }

  updateItems(items) {
    this.items = items;
    localStorage.setItem('shopping-cart', btoa(JSON.stringify(this.items)));
  }

  clearItems() {
    this.items = [];
    localStorage.setItem('shopping-cart', btoa(JSON.stringify([])));
  }
}
