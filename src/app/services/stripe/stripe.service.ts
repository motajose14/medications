import { Injectable } from '@angular/core';
import { StripeService as StripeController } from 'ngx-stripe';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(
    private stripeCtrl: StripeController
  ) { }

  createToken(card,name){
    return this.stripeCtrl.createToken(card,{name});
  }

  paymentIntent(){
  }

}
