import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PrintServicesService {

  isPrinting:boolean=false;

  constructor(private router: Router) {
   }

   printDocument(data){
    this.isPrinting=true;
    this.router.navigate([{outlets:{'print':['print',data]}}]);
   }

   onDataReady(){
    setTimeout(() => {
     this.isPrinting=false;
     this.router.navigate([{outlets:{print:null}}]);
    },100);
  }
}
