import { TestBed } from '@angular/core/testing';

import { PrintServicesService } from './print-services.service';

describe('PrintServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrintServicesService = TestBed.get(PrintServicesService);
    expect(service).toBeTruthy();
  });
});
