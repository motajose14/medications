import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IsLoginService {

  logginStatus:boolean= false;

  constructor(
    private http: HttpClient
  ) {
    this.local();
  }

  local() {
    const value = localStorage.getItem('loggin');
    if (value === 'false' || value === null || value === undefined) {
      this.logginStatus = false;
    } else {
      this.logginStatus = true;
    }
  }
  setLoggin(value: boolean) {
    this.logginStatus = value;
  }

  getLoggin(){
    return this.logginStatus;
  }

  getRow(){
    var datas=JSON.parse(localStorage.getItem('data'));
    return datas.row;
  }

}
