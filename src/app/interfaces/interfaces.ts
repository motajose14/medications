import { StringifyOptions } from 'querystring';

export  interface IUsers {
    id: string;
    username: string;
}

export interface IResp {
    result?: boolean;
    msg?: string;
    option?: number;
    log?: ILog;
    row?: string;
}
export interface ILog {
    data: string;
    row: string;
}

export interface IDataUser {
    info?: IUserInfo;
    address?: IUserAddress[];
    pay_methods?: IUserPayMethods[];
}

export interface IUserInfo {
    first_name?: string;
    last_name?: string;
    email?: string;
    mobile?: string;
    gender?: string;
    birth_day?: string;
    location?: string;
    type?: string;
}

export interface IUserAddress {
    id?: string;
    address_type?: string;
    street1?: string;
    street2?: string;
    city?: string;
    state?: string;
    zip_code?: string;
    status?: string;
}

export interface IUserPayMethods {
    id?: string;
    data: string | any;
    status?: string;
    id_customer?: string;
}

export interface IItem {
    id?: number;
    label?: string;
}

export interface IDrugInfo {
    dosageform?: string;
    strength?: string;
    qty?: string;
    did?: string;
    dnd?: string;
    brandinfo?: IBrandsInfo[];
    genericinfo?: string;
    generalinfo?: IGeneralInfo[];
    mrp_pricinginfo?: IMrpPricingInfo[];
    dosageforms?: IDosageForms[];
    defaultstrengths?: IDefaultStrengths[];
    quantities?: IQuantities[];
    asp?: string;
}


export interface IMrpPricingInfo {
    cid?: string;
    cname?: string;
    caddress?: string;
    ccity?: string;
    cstate?: string;
    czip?: string;
    cprice?: string;
    name?: string;
}

export interface IGeneralInfo {
    id: string;
    commonname: string;
    genericbrand: string;
    generics: string;
    brands: string;
    slug: string;
    dnd: string;
}

export interface IBrandsInfo {
    id?: string;
    dnd?: string;
    commonname?: string;
    genericbrand?: string;
    genereics?: string;
    brands?: string;
    slug?: string;
}

export interface IDosageForms {
    dosage?: string;
}

export interface IDefaultStrengths {
    strength?: string;
}

export interface IQuantities {
    id?: string;
    label_type?: string;
    default_quantity?: string;
    form?: string;
    default_dosage?: string;
    default_dosage_form?: string;
    quantities?: string;
    uniqId?: string;
    label?: string;
    dosage?: string;
    url?: string;
    slug?: string;
    timestamp?: string;
    quantity?: IQuantity[];
}

export interface IQuantity {
    quantity?: string;
}

export interface IShopping {
    id?: string;
    drugname?: string;
    qty?: number;
    price?: number;
    pharmacy_id?: string;
    pharmacy_name?: string;
    pharmacy_address?: string;
    pharmacy_tel?: string;
    brand: string;
    dosage: string;
    strength: string;
    quantity: string;
}

export interface ICoins {
    coins?: number;
    amts?: string;
    orderids?: string;
    id?: string;
    tradeinson?: string;
}

export interface UCoins {
    amts?: number;
    orderids?: string;
    description?: string;
}

export interface IUCoins {
    uid?: string;
    balance?: number;
    id?: string;
    coins?: ICoins[];
    ucoins?: UCoins[];
    tradeinson?: string;
}

export interface IMyWishlist {
    id?: string;
    user_id?: string;
    drug_name?: string;
    med_id?: string;
    pharmacy_name?: string;
    pharmacy_address?: string;
    brand?: string;
    dosage?: string;
    strength?: string;
    quantity?: string;
    pharmacy_id?: string;
    price?: number;
}

export interface ICerts {
    orderdet_id?: string;
    user_id?: string;
    orderid?: string;
    phorderid?: string;
    more_info?: string;
    id_charge?: StringifyOptions;
    coins?: number;
    amount?: number;
    stripe_charge_det?: string | any;
    netamount?: number;
    date?: Date;
    itemprice?: number;
    qty?: number;
    pharmacy_id?: string;
    orderon?: number;
    status?: string;
    cert_no?: string;
    pin_no?: string;
    expiredon?: number;
    redeemon?: number;
    disputeon?: number;
    disputenotes?: string;
    resol?: string;
    resolon?: number;
    product?: string;
    productinfo?: IShopping;
    tradedon?: number;
    paid?: number;
    paidon?: number;
    ipaddress?: string;
    comper?: string;
    commission?: string;
    adminnote?: string;
    sellernote?: string;
    paystatus?: string;
    disputefrom?: string;
    prodid?: string;
}


