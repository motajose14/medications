import { Directive } from '@angular/core';

import { ValidatorFn, FormGroup, ValidationErrors, Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
export const passwordMatch: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const password = control.get('password');
  const repassword = control.get('repassword');

  return password.value !== repassword.value ? { 'repass': true } : null;
};


@Directive({
  selector: '[appPasswordMatch]',
  providers: [{ provide: NG_VALIDATORS, useExisting: PasswordMatchDirective, multi: true }]
})


export class PasswordMatchDirective implements Validator {

  validate(control: AbstractControl): ValidationErrors {
    return passwordMatch(control);
  }

}

