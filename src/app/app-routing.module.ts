import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ResultsComponent } from './pages/results/results.component';
import { DetailsComponent } from './pages/details/details.component';
import { PayComponent } from './pages/pay/pay.component';
import { PrintComponent } from './pages/print/print.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { IsLoginGuard } from './guards/is-login.guard';
import { IsAuthGuard } from './guards/isAuth/is-auth.guard';
import { RegisterComponent } from './pages/register/register.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { WhislistComponent } from './pages/whislist/whislist.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { AccountComponent } from './pages/account/account.component';
import { DetailCertificateComponent } from './pages/detail-certificate/detail-certificate.component';
import { ResetComponent } from './pages/reset/reset.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'results/:drug_name', component: ResultsComponent},
  {path: 'results/:drug_name/:location', component: ResultsComponent},
  {path: 'results/:drug_name/:location/:state', component: ResultsComponent},
  {path: 'details/:drug_id', component: DetailsComponent},
  {path: 'pay', component: PayComponent, canActivate: [IsLoginGuard]},
  {path: 'login', component: LoginComponent, canActivate: [IsAuthGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [IsAuthGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [IsLoginGuard]},
  {path: 'wishlist', component: WhislistComponent, canActivate: [IsLoginGuard]},
  {path: 'account', component: AccountComponent, canActivate: [IsLoginGuard]},
  {path: 'reviews', component: ReviewsComponent, canActivate: [IsLoginGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [IsLoginGuard]},
  {path: 'detail/certificate/:id', component: DetailCertificateComponent, canActivate: [IsLoginGuard]},
  {path: 'forgot', component: ForgotComponent},
  {path: 'password-reset/:token', component: ResetComponent},
  {
    path: 'print/:data',
    component: PrintComponent, outlet: 'print'
  },
  {path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
